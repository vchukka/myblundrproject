package com.abonda;
/**
 * This is the User class it declears data types and data values in this application
 * @author vchukka
 */

public class User {
	String  name;
	int     age;
	String  profession;
	String  hobbie;
	/**
	 * This is helper method to create name,age,profession and hobbies parameter 
	 * @param name
	 * @param age
	 * @param profession
	 * @param hobbie
	 */
	//parameterized constructor
public User(String name,int age,String profession,String hobbie)
	{
		this.name       = name;        //representation of name
		this.age        = age;         //representation of age
		this.profession = profession;  //representation of profession
		this.hobbie     = hobbie;      //representation of hobbie
		System.out.println(this);
	}
    //to string method
	@Override
public String toString() {
		String returnstring = "--------------";
		returnstring = "name         :"+this.name+"\n";
		returnstring = "age          :"+this.age+"\n";
		returnstring = "profession   :"+this.profession+"\n";
		returnstring = "hobbie       :"+this.hobbie+"\n";

		return returnstring; 
	}
	//getters method
	public String getHobbie() {
		return hobbie;
	}
	
	}

