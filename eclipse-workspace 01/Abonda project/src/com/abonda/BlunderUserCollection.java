package com.abonda;

import java.util.ArrayList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is userCollection class to do mass operations on all users
 * @author vchukka
 *
 */
public class BlunderUserCollection {
	//Dynamic list to contain all users in the system
	private ArrayList<User> userList;
	
	public BlunderUserCollection() {
		this.userList = new ArrayList<User>();
	}
	/**
	 * This is helper method to create name,age,profession and hobbies parameter
	 * @param name
	 * @param age
	 * @param profession
	 * @param hobbie
	 */
	public boolean createNewBlunderUserCollection(String name,int age,String profession,String hobbie) {
		
		User newBlunderUserCollection =new User(name,age,profession,hobbie);
	     this.userList.add(newBlunderUserCollection);
		 return true;
		
	}
private boolean validatehobbie(String hobbie) {
		
		Pattern pattern = Pattern.compile("^[A-Za-z ]{3,7}$");
	    Matcher matcher = pattern.matcher(hobbie);
	    if(matcher.find()) {
	    	// now checking for duplicates
	    	boolean isDuplicateFound = false;
	    	for (int i = 0; i < this.userList.size(); i++) {
				User currentUser = this.userList.get(i);
				if (currentUser.getHobbie().toLowerCase().trim().equals(hobbie.toLowerCase().trim())) {
					isDuplicateFound = true;
				}
			}
	    	if (isDuplicateFound) {
	    		return false; // duplicate hobbie not allowed
	    	} else {
	    		return true;
	    	}
	    } else {
	    	return false;
	    }
	}
	
}
