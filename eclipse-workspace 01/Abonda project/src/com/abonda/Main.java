package com.abonda;
/**
 * Main class contains main method to test various features during development;
 * @author vchukka
 */
public class Main {
	/**
	 * Main method to test various features
	 */
public static void main(String[]args) {
		Main myusertest = new Main();
		myusertest.testuserCollection();
	}
	/**
	 * Instance method to test user object creation
	 */
		private void testuserCollection() {
			BlunderUserCollection bc = new BlunderUserCollection();
			bc.createNewBlunderUserCollection("krishna", 23,"java developer","playing");
			bc.createNewBlunderUserCollection("kalpana", 22,"java developer","sleeping");
		}
		
	}

